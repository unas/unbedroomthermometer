import RPi.GPIO as GPIO
import sys
import time
import datetime

LD1 = 14
LD2 = 15
LD3 = 18

LEDA = 23
LEDB = 24
LEDC = 25
LEDD = 8

C = 16
F = 12
MINUS = 7

CELSIUS = 1
KELVIN = 2
FAHRENHEIT = 3

def Init():
	print("Initializing...")
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(LEDC, GPIO.OUT) # LED C
	GPIO.setup(LEDB, GPIO.OUT) # LED B
	GPIO.setup(LEDD, GPIO.OUT) # LED D
	GPIO.setup(LEDA, GPIO.OUT) # LED A
	GPIO.setup(LD1, GPIO.OUT) # LED1 LD
	GPIO.setup(LD2, GPIO.OUT) # LED2 LD
	GPIO.setup(LD3, GPIO.OUT) # LED3 LD
	GPIO.setup(C, GPIO.OUT) # C temp sign
	GPIO.setup(F, GPIO.OUT) # Fahrenheit F-leg
	GPIO.setup(MINUS, GPIO.OUT)  # Minus sign
	
	# First turning all LDs on, so that the digit's can be zeroed
	GPIO.output(LD1, 1)
	GPIO.output(LD2, 1)
	GPIO.output(LD3, 1)
	
	# Putting zero on all LEDs
	GPIO.output(LEDA, 0)
	GPIO.output(LEDB, 0)
	GPIO.output(LEDC, 0)
	GPIO.output(LEDD, 0)
	
	# Closing minus and temperature sign
	GPIO.output(C, 0)
	GPIO.output(F, 0)
	GPIO.output(MINUS, 0)
	
	# Turning LDs back off
	GPIO.output(LD1, 0)
	GPIO.output(LD2, 0)
	GPIO.output(LD3, 0)
	
	# By default, led1 is turned of, unless showing kelvin, but it will be turned back on later if needed
	CloseLED(1)
	print("initialization complete")

def CloseLED(LED):
	# The led can be closed, by setting a binary value larger than 1001
	# Setting value 1111
	
	# Setting number
	GPIO.output(LEDA, 1)
	GPIO.output(LEDB, 1)
	GPIO.output(LEDC, 1)
	GPIO.output(LEDD, 1)
	
	if (LED == 1):
		# Turning LD momentary on
		GPIO.output(LD1, 1)
		#Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD momentary on
		GPIO.output(LD2, 1)
		#Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD momentary on
		GPIO.output(LD3, 1)
		#Turning LD back off
		GPIO.output(LD3, 0)

# Converts decimal number to binary number
def IntToBinArray(value):
	returnValue = [0, 0, 0, 0]
	if (value > 9):
		return returnValue

	binaryStr = str(bin(value)[2:])
	# If the binaryStr is shorter than 3 characters, then adding zeros to the beginning 111 => 0111
	if (len(binaryStr) < len(returnValue)):
		zeroAmount = len(returnValue) - len(binaryStr)
		for i in range(zeroAmount):
			binaryStr = '0' + binaryStr

	for i in range(len(binaryStr)):
		returnValue[i] = int(binaryStr[i])

	return returnValue
	
def SetNegative(negative):
	GPIO.output(MINUS, negative)
	
def SetLEDValue(LED, number):
	# Converting number to binary array
	binArray = IntToBinArray(number)
	if (len(binArray) != 4): # Has to be 4-bit value
		print("Error, number is not 4-bit")
		return 0
		
	GPIO.output(LEDD, binArray[0]) # D
	GPIO.output(LEDC, binArray[1]) # C
	GPIO.output(LEDB, binArray[2]) # B
	GPIO.output(LEDA, binArray[3]) # A
	
	if (LED == 1):
		# Turning LD on
		GPIO.output(LD1, 1)
		# Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD on
		GPIO.output(LD2, 1)
		# Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD on
		GPIO.output(LD3, 1)
		# Turning LD back off
		GPIO.output(LD3, 0)

def SetNumber(number, type):
	if (type == CELSIUS or type == FAHRENHEIT):
		if (number > 99 or number < -99):
			print 'Error: Number can\'t be larger than 2 digits'
			return 0
	elif (type == KELVIN):
		if (number > 999 or number < 0):
			print 'Error: Number can\'t be larger than 3 digits or negative'
			return 0
	
	numberStr = str(abs(number))
	negative = False

	# If value has less than 2 digits, then zeros will be added 6 => 06, Kelvin 6 => 006
	if (type == CELSIUS or type == FAHRENHEIT):
		if (number < 0): # If number is negative
			negative = True
		if (len(numberStr) < 2):
				numberStr = '0' + numberStr
	elif (type == KELVIN):
		for i in range(3 - len(numberStr)):
			numberStr = '0' + numberStr
		# For kelvin, setting first digit
		SetLEDValue(1, int(numberStr[0]))
	
	# Negative index, so that it doesn't matter if we have 3 digits (kelvin) or 2 digits
	SetLEDValue(2, int(numberStr[-2]))
	SetLEDValue(3, int(numberStr[-1]))
	
	# Turning negative value On/Off)
	SetNegative(negative)

def SetType(type):
	if (type == CELSIUS):
		GPIO.output(C, True)
		GPIO.output(F, False)
	elif (type == FAHRENHEIT):
		GPIO.output(C, True)
		GPIO.output(F, True)
	elif (type == KELVIN):
		# Turning C and F sign off
		GPIO.output(C, False)
		GPIO.output(F, False)
		# Turning the first led on, since we need 3 digits for kelvin values
		SetLEDValue(1, 0)

def main(argv):
	try:
		print("Starting in...")
		for i in range(10, -1, -1):
			print(i)
			time.sleep(1)

		# Initializing pins
		Init()
		
		# Setting C / K / F sign
		type=KELVIN
		SetType(type)
		print("Type is %s", str(type))
		
		while(True):
			print("Closing all")
			CloseLED(1)
			CloseLED(2)
			CloseLED(3)
			time.sleep(2)
			print("Counting one led at a time")
			for i in range(3,0,-1):
				for j in range(10):
					SetLEDValue(i, j)
					time.sleep(0.5)
				CloseLED(i)
			print("Counting 2 leds")
			CloseLED(1)
			SetLEDValue(3, 0)
			SetLEDValue(2, 0)
			for i in range(-99, 100):
				SetNumber(i, CELSIUS)
				time.sleep(0.1)
			
			print("Counting all leds")
			for i in range(0, 1000):
				SetNumber(i, KELVIN)
				time.sleep(0.05)
			CloseLED(1)
			CloseLED(2)
			CloseLED(3)
			print("Testing symbols")
			for i in range(0,10):
				SetType(KELVIN)
				time.sleep(0.5)
				SetType(CELSIUS)
				time.sleep(0.5)
				SetType(FAHRENHEIT)
				time.sleep(0.5)
			SetType(KELVIN)
	finally:
		CloseLED(1)
		CloseLED(2)
		CloseLED(3)
		GPIO.cleanup()
		
	
if __name__ == "__main__": # Checking if this is the main program, not an imported module
    main(sys.argv)

