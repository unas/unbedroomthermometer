import time
import datetime
import RPi.GPIO as GPIO
import Adafruit_MCP9808.MCP9808 as MCP
import sys
import peewee
from peewee import *
import threading
import logging
import socket
import json
import re

class NightTime:
	minTime = ["6:00", "6:00", "6:00", "6:00", "6:00", "6:00", "6:00"]
	maxTime = ["21:00", "21:00", "21:00", "21:00", "22:30", "22:30", "21:00"]

logging.basicConfig(filename='thermometer.log',level=logging.INFO,format='%(asctime)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

LD1 = 14
LD2 = 15
LD3 = 18

LEDA = 23
LEDB = 24
LEDC = 25
LEDD = 8

C = 16
F = 12
MINUS = 7

CELSIUS = 1
KELVIN = 2
FAHRENHEIT = 3

class Temperature(Model):
	id = PrimaryKeyField(primary_key=True)
	date = DateTimeField(default=datetime.datetime.now)
	tempC = DecimalField(max_digits=6, decimal_places=2)
	tempF = DecimalField(max_digits=6, decimal_places=2)
	tempK = DecimalField(max_digits=6, decimal_places=2)
	
	class Meta:
		database = MySQLDatabase('thermometer', host='10.0.1.5', port=3306, user='thermometer',passwd='thermometer')

class FuncThread(threading.Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        threading.Thread.__init__(self)
 
    def run(self):
        self._target(*self._args)

def Init():
	logging.info("Initializing...")
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(LEDC, GPIO.OUT) # LED C
	GPIO.setup(LEDB, GPIO.OUT) # LED B
	GPIO.setup(LEDD, GPIO.OUT) # LED D
	GPIO.setup(LEDA, GPIO.OUT) # LED A
	GPIO.setup(LD1, GPIO.OUT) # LED1 LD
	GPIO.setup(LD2, GPIO.OUT) # LED2 LD
	GPIO.setup(LD3, GPIO.OUT) # LED3 LD
	GPIO.setup(C, GPIO.OUT) # C temp sign
	GPIO.setup(F, GPIO.OUT) # Fahrenheit F-leg
	GPIO.setup(MINUS, GPIO.OUT)  # Minus sign
	
	# First turning all LDs on, so that the digit's can be zeroed
	GPIO.output(LD1, 1)
	GPIO.output(LD2, 1)
	GPIO.output(LD3, 1)
	
	# Putting zero on all LEDs
	GPIO.output(LEDA, 0)
	GPIO.output(LEDB, 0)
	GPIO.output(LEDC, 0)
	GPIO.output(LEDD, 0)
	
	# Closing minus and temperature sign
	GPIO.output(C, 0)
	GPIO.output(F, 0)
	GPIO.output(MINUS, 0)
	
	# Turning LDs back off
	GPIO.output(LD1, 0)
	GPIO.output(LD2, 0)
	GPIO.output(LD3, 0)
	
	# By default, led1 is turned of, unless showing kelvin, but it will be turned back on later if needed
	CloseLED(1)
	
	logging.info("initialization complete")

def TCPServer():
	# Values used for returning TCP requests, must be set as global
	global TEMPC
	global TEMPK
	global TEMPF
	
	TEMPC = 0
	TEMPK = 0
	TEMPF = 0
	
	TCP_IP = '10.0.2.9' # The port of the server
	TCP_PORT = 5015
	BUFFER_SIZE = 10240  # Max chars for message
	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.bind((TCP_IP, TCP_PORT))
	print 'starting listen'
	s.listen(1)
	print 'listen started'
	while 1:
		conn, addr = s.accept()
		try:
			print 'Connection address:', addr[0]
			while 1:
				data = conn.recv(BUFFER_SIZE)
				if not data: break
				print "received data: \"" + repr(data.strip()[2:]) + "\""
				# [2:] removes the first 2 chars, which are hex-values if message is from java,
				# since writeUTF add's 2 hex-values telling the length of the string
				if (data.strip()[2:] == "getTemperature" or data.strip() == "getTemperature"):
					returnJSON = json.JSONEncoder().encode({
					"success": True,
					"message": "",
					"temperatureC": TEMPC,
					"temperatureK": TEMPK,
					"temperatureF": TEMPF
					})
					conn.send(str(returnJSON + "\n"))
				else:
					returnJSON = json.JSONEncoder().encode({
					"success": False,
					"message": "Unknown request!"
					})
					print "Sending", returnJSON
					conn.send(str(returnJSON) + "\n")
		finally:
			conn.close()

	
def CloseLED(LED):
	# The led can be closed, by setting a binary value larger than 1001
	# Setting value 1111
	
	# Setting number
	GPIO.output(LEDA, 1)
	GPIO.output(LEDB, 1)
	GPIO.output(LEDC, 1)
	GPIO.output(LEDD, 1)
	
	if (LED == 1):
		# Turning LD momentary on
		GPIO.output(LD1, 1)
		#Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD momentary on
		GPIO.output(LD2, 1)
		#Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD momentary on
		GPIO.output(LD3, 1)
		#Turning LD back off
		GPIO.output(LD3, 0)

# Converts decimal number to binary number
def IntToBinArray(value):
	returnValue = [0, 0, 0, 0]
	if (value > 9):
		return returnValue
	
	binaryStr = str(bin(value)[2:])
	# If the binaryStr is shorter than 3 characters, then adding zeros to the beginning 111 => 0111
	if (len(binaryStr) < len(returnValue)):
		zeroAmount = len(returnValue) - len(binaryStr)
		for i in range(zeroAmount):
			binaryStr = '0' + binaryStr
	
	for i in range(len(binaryStr)):
		returnValue[i] = int(binaryStr[i])
	
	return returnValue

# Set's a specific LED's number
def SetLEDValue(LED, number):
	# Converting number to binary array
	binArray = IntToBinArray(number)
	if (len(binArray) != 4): # Has to be 4-bit value
		logging.error("Error, number is not 4-bit")
		return 0
	
	GPIO.output(LEDD, binArray[0]) # D
	GPIO.output(LEDC, binArray[1]) # C
	GPIO.output(LEDB, binArray[2]) # B
	GPIO.output(LEDA, binArray[3]) # A
	
	if (LED == 1):
		# Turning LD on
		GPIO.output(LD1, 1)
		# Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD on
		GPIO.output(LD2, 1)
		# Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD on
		GPIO.output(LD3, 1)
		# Turning LD back off
		GPIO.output(LD3, 0)

def SetNegative(negative):
	GPIO.output(MINUS, negative)

def SetNumber(number, type, debug = False):
	roundNumber = int(round(number, 0))
	if (type == CELSIUS or type == FAHRENHEIT):
		if (roundNumber > 99 or roundNumber < -99):
			logging.info('Error: Number can\'t be larger than 2 digits')
			return 0
	elif (type == KELVIN):
		if (roundNumber > 999 or roundNumber < 0):
			logging.info('Error: Number can\'t be larger than 3 digits or negative')
			return 0
	
	numberStr = str(abs(roundNumber))
	negative = False

	# If value has less than 2 digits, then zeros will be added 6 => 06, Kelvin 6 => 006
	if (type == CELSIUS or type == FAHRENHEIT):
		if (number < 0): # If number is negative
			if (debug == True):
				logging.info("Setting negative sign: %s", str(number))
			negative = True
		elif(debug == True):
			logging.info("Setting positive sign: %s", str(number))
		if (len(numberStr) < 2):
			numberStr = '0' + numberStr
	elif (type == KELVIN):
		for i in range(3 - len(numberStr)):
			numberStr = '0' + numberStr
		# For kelvin, setting first digit
		SetLEDValue(1, int(numberStr[0]))
	
	# Negative index, so that it doesn't matter if we have 3 digits (kelvin) or 2 digits
	SetLEDValue(2, int(numberStr[-2]))
	SetLEDValue(3, int(numberStr[-1]))
	
	# Turning negative value On/Off)
	SetNegative(negative)

def SetType(type):
	if (type == CELSIUS):
		GPIO.output(C, True)
		GPIO.output(F, False)
	elif (type == FAHRENHEIT):
		GPIO.output(C, True)
		GPIO.output(F, True)
	elif (type == KELVIN):
		# Turning C and F sign off
		GPIO.output(C, False)
		GPIO.output(F, False)
		# Turning the first led on, since we need 3 digits for kelvin values
		SetLEDValue(1, 0)
		

unsavedTemps = []
def SaveTempToDatabase(tempC, tempF, tempK, debug):
	if (SKIPDB == True):
		logging.info("Skipping database save")
		return
	try:
		# Making sure, there aren't old temps that need to be saved first
		CheckUnsavedTemps()
		
		logging.info('Saving %sC/%sF/%sK to database', str(tempC), str(tempF), str(tempK))
		temperature = Temperature()
		temperature.tempC = tempC
		temperature.tempF = tempF
		temperature.tempK = tempK
		temperature.save()
		logging.info("Values saved to database successfully")
	except:
		logging.critical("Values could not be saved to the database! Trying to save later...")
		failedTemp = [datetime.datetime.now(), tempC, tempF]
		unsavedTemps.append(failedTemp)

def CheckUnsavedTemps():
	if (len(unsavedTemps) > 0):
		logging.info("There are %s unsaved temperatured. Trying to save them now", str(len(unsavedTemps)))
		SaveUnSavedTempsToDatabase()
	else:
		logging.info("There are no unsaved temps, continuing...")

def SaveUnSavedTempsToDatabase():
	try:
		# Loop from array length - 1, to 0, step -1. So from e.g 10 - 0
		for i in range((len(unsavedTemps) - 1), -1, -1):
			logging.info("Trying to save unSavedValue %s %sC %sF", unsavedTemps[i][0], unsavedTemps[i][1], unsavedTemps[i][2])
			oldTemperature = Temperature()
			oldTemperature.date = unsavedTemps[i][0]
			oldTemperature.tempC = unsavedTemps[i][1]
			oldTemperature.tempF = unsavedTemps[i][2]
			oldTemperature.save()
			# Removing the value from the list
			del unsavedTemps[-1] # Removes the last part
			logging.info("%s %sC %sF saved successfully", unsavedTemps[i][0], unsavedTemps[i][1], unsavedTemps[i][2])
		logging.info("All unsaved temperatures are now saved to the database")
	except:
		logging.info("Couldn't save value that wasn't saved earlier. Trying again in an hour")

def turnLEDSOff():
	GPIO.output(LEDD, 1) # D
	GPIO.output(LEDC, 1) # C
	GPIO.output(LEDB, 1) # B
	GPIO.output(LEDA, 1) # A
	
	GPIO.output(LD1, 1)
	GPIO.output(LD1, 0)
	GPIO.output(LD2, 1)
	GPIO.output(LD2, 0)
	GPIO.output(LD3, 1)
	GPIO.output(LD3, 0)
	GPIO.output(MINUS, False)
	GPIO.output(C, False)
	GPIO.output(F, False)

def isNightTime():
	weekday = datetime.datetime.now().weekday()
	now = datetime.datetime.strptime(str(datetime.datetime.now().hour) + ":" + str(datetime.datetime.now().minute), "%H:%M")
	minTime = datetime.datetime.strptime(NightTime.minTime[weekday], "%H:%M")
	maxTime = datetime.datetime.strptime(NightTime.maxTime[weekday], "%H:%M")
	
	return now < minTime or now >= maxTime

def main(argv):

	# Starting tcp server
	TCPServerThread = FuncThread(TCPServer)
	# When threads are daemons, they will be killed with ctrl-c, not just the main program
	TCPServerThread.daemon = True
	TCPServerThread.start()
	
	# Creating global, so that the thread would see this too
	global TEMPC
	global TEMPK
	global TEMPF
	global SKIPDB
	
	# If this turns to false, then there was an error and the program needs to start again
	success = True
	try:
		logging.info("Starting thermometer program....")
	
		# Checking if debug is on
		debug = False
		type = CELSIUS
		SKIPDB = False
		for i in range(0, len(argv)):
			if (argv[i] == '-d' or argv[i] == '--debug'):
				debug = True
			elif (argv[i] == '-k' or argv[i] == '--kelvin'):
				type = KELVIN
			elif (argv[i] == '-f' or argv[i] == '--fahrenheit'):
				type = FAHRENHEIT
			elif (argv[i] == '--skipdb'):
				SKIPDB = True
		
		# Initializing pins
		Init()
		
		# Setting C / K / F sign
		SetType(type)
		logging.info("Type is %s", str(type))
		
		# Making sure, that the table exists
		if (Temperature.table_exists() == False): # Or, create_table(True), will not give an exception
			logging.info("Table doesn't exist, creating...")
			Temperature.create_table()
		
		# Sensor for MCP9808
		sensor = MCP.MCP9808()
		# Initialize communication with the sensor.
		sensor.begin()

		# Sensor readings
		temps = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
		# How often to update the temperature (sec)
		updateFreq = 10
		
		# Getting latest value from database. If it has been more then an hour ago, then saving current temps
		dbSaveTime = datetime.datetime.now()
		temp = Temperature.select().order_by(Temperature.id.desc()).limit(1) # Getting newest db save time
		if (temp.count() == 0): # If there are no old values (empty table)
			logging.info('Table is empty, starting from scratch')
			logging.info('dbSaveTime: %s', str(dbSaveTime))
			temperature = sensor.readTempC()
			CRound = round(temperature, 2)
			FRound = round((temperature * 9/5 + 32), 2)
			KRound = round((temperature + 273.15), 2)
			databaseThread = FuncThread(SaveTempToDatabase, CRound, FRound, KRound, debug)
			databaseThread.start();
		else:
			# Checking if it has been over an hour
			logging.info('Newest date is %s and current is %s', str(temp[0].date), str(datetime.datetime.now()))
			if ( ((datetime.datetime.now() - temp[0].date).total_seconds() / 3600) >= 1):
				# Has been more than an hour ago. Saving.
				logging.info('Has been over an hour, saving')
				temperature = sensor.readTempC()
				CRound = round(temperature, 2)
				FRound = round((temperature * 9/5 + 32), 2)
				KRound = round((temperature + 273.15), 2)
				databaseThread = FuncThread(SaveTempToDatabase, CRound, FRound, KRound, debug)
				databaseThread.start();
			else:
				logging.info('Hasn\'t been over an hour since last save')
				logging.info('dbSaveTime is now %s', str(temp[0].date))
				dbSaveTime = temp[0].date
		
		# Loop printing measurements every second. Showing average after 10 loops
		logging.info("Starting main loop")
		while True:
			for i in range(0, len(temps)): # 0 - 9
				temp = sensor.readTempC()
				# Saving the temperature in the array
				temps[i] = temp;
				if (debug == True):
					if (type == CELSIUS):
						logging.info('Temperature: %sC', str(temp))
					elif (type == KELVIN):
						logging.info('Temperature: %sK', str(temp + 273.15))
					elif (type == FAHRENHEIT):
						logging.info('Temperature: %sF', str(temp * 9/5 + 32))
				time.sleep(updateFreq / len(temps))
			
			averageC = sum(temps) / len(temps)
			averageF = (sum(temps) / len(temps)) * 9/5 + 32
			averageK = (sum(temps) / len(temps)) + 273.15
			averageCRound = round(averageC, 2)
			averageFRound = round(averageF, 2)
			averageKRound = round(averageK, 2)

			if (isNightTime()):
				turnLEDSOff()
			else:
				SetType(type)
				if (type == CELSIUS):
					SetNumber(averageC, type, debug)
				elif (type == KELVIN):
					SetNumber(averageK, type, debug)
				elif (type == FAHRENHEIT):
					SetNumber(averageF, type, debug)
			
			if (debug == True):
				if (type == CELSIUS):
					logging.info('Average temp: %sC', str(averageCRound))
				elif (type == KELVIN):
					logging.info('Average temp: %sK', str(averageCRound))
				elif (type == FAHRENHEIT):
					logging.info('Average temp: %sF', str(averageFRound))
					
			# Checking if it has been an hour since we last saved the temperature to the database
			if ( ((datetime.datetime.now() - dbSaveTime).total_seconds() / 3600) >= 1):
				databaseThread = FuncThread(SaveTempToDatabase, averageCRound, averageFRound, averageKRound, debug)
				databaseThread.start()
				# Saving time, so that we can save again after an hour
				dbSaveTime = dbSaveTime + datetime.timedelta(hours=1)
				if (debug == True):
					logging.info('Has been an hour since last save')
					logging.info('New dbSaveTime: %s', str(dbSaveTime))
			else:
				if (debug == True):
					logging.info('Hasn\'t been an hour yet')
					
			# Setting values for the socket server
			TEMPC = averageC
			TEMPK = averageK
			TEMPF = averageF
			
	except Exception,e:
		logging.error("Error in main loop: " + str(e))
		success = False
	finally:
		logging.info("Program shutting down...")
		logging.info("Cleaning GPIO")
		GPIO.cleanup()
		CheckUnsavedTemps()
		
		# Returning value. If exiting with an error, then the main program will be restarted
		return success
	
if __name__ == "__main__": # Checking if this is the main program, not an imported module
    while True:
		returnValue = main(sys.argv)
		if (returnValue == True): # Exiting only if main didn't stop due to an error
			logging.info("Clean exit, shutting program down...")
			break
		else: # Main crashed due to an error. This is propably because an I/O error. This error is usually fixed by itself. Waiting 1 second and then starting main again
			logging.error("Error, restarting in 1 second...")
			time.sleep(1)
